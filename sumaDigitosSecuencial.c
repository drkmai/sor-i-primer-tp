#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

int sumaTotal=0;
int numberArray[10000];

int main()
{
  FILE *myFile;
  myFile = fopen("10milDigitosDePi_separados.txt", "r");

  //leer archivo en un array

  int i;

  if (myFile == NULL)
  {
    printf("Error en la lectura\n");
    exit (0);
  }

  for (i = 0; i < 10000; i++)
  {
    fscanf(myFile, "%d,", &numberArray[i] );
  }

    printf("suma secuencial\n");
    int suma = 0;
    for(i = 0; i < 10000; i++)
    {
        suma +=numberArray[i];	
    }
    printf("la suma da %i\n",suma);


    fclose(myFile);

    return 0;
}