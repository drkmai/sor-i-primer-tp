#include <stdio.h>
#include <stdlib.h>
#include <string.h> // para strcmp()
#include <unistd.h> // para fork(), pid_t
#include <sys/wait.h> // para  waitpid() y WIF

int salir = 0;

#define MAX_COMMAND_LENGTH 100
char cmd[MAX_COMMAND_LENGTH];

#define MAX_NUMBER_OF_PARAMS 10
char* params[MAX_NUMBER_OF_PARAMS + 1];

void parseCmd(char* cmd, char** params)
{
    for(int i = 0; i < MAX_NUMBER_OF_PARAMS; i++)
    {
        params[i] = strsep(&cmd, " ");
        if(params[i] == NULL) break;
    }
}

int correrCommando(char **args)
{
	pid_t pid, wpid;
	int status;
	pid = fork();
	if (pid == 0) // Proceso hijo
	{

		/* reemplaza la process image del proceso que llama el execvp con una nueva process image.
			Esto hace que se corra un nuevo programa con el PID del proceso que llama al execvp.
		*/
		if (execvp(args[0], args) == -1)
		{
			perror("Shell grupo5 ");
		}
		exit(EXIT_FAILURE);
	}
	else if (pid < 0) //Error en el fork
	{
		perror("Shell grupo5 ");
	}
	else // proceso padre
	{	
		do 
		{
			/*waitpid() suspende la ejecuccion del proceso hasta que el proceso hijo
			especificado por pid en el arg haya cambiado de estado*/
			wpid = waitpid(pid, &status, WUNTRACED); 
		} while (!WIFEXITED(status) && !WIFSIGNALED(status));
		//WIFEXITED: devuelve un valor distinto de 0 si el proceso hijo termino normalmente.
		// WIFSIGNALED: devuelve un valor distinto de 0 si el proceso hijo recibio una señal no manejada.
		// Ejemplo: una señal de error critico, como una division por 0.
	}
	return 1;
}

int ejecutar(char **args)
{
	
	if (args[0] == NULL)
	{
		// Comando vacio
		return 1;
	}
	
	if(strcmp(args[0], "cd")==0) // Compara el comando con cd.
	{
		if (args[1] == NULL) 
		{
			printf("Shell grupo5: expected argument to \"cd\"\n");
		} 
		else 
		{
			/*chdir es un system call usado para cambiar el directorio de trabajo.
			Es una alias del comando shell 'cd'*/
			if (chdir(args[1]) != 0) 
			{
				perror("Shell grupo5: ");
			}
		}
		return 1;
	}
	
	if (strcmp(args[0],"exit")==0) //compara el comando con exit.
	{
		salir = 1;
		return 1;
	}
	int ret;
	ret = correrCommando(args);
	return ret;
}

int main()
{
	while(salir==0)
	{
		printf("Shell grupo5$ ");
		fgets(cmd, sizeof(cmd), stdin); //toma el imput y lo mete en cmd

        //remover el enter del comando
		if(cmd[strlen(cmd)-1] == '\n')
        {
            cmd[strlen(cmd)-1] = '\0';
        }
        // dividir el comando en un array de parámetros
        parseCmd(cmd, params);
		//lo ejecuta.
		ejecutar(params);
	}
	return 0;
}