#include <stdio.h>//para hacer printf
#include <stdlib.h>//para usar exit y funciones de libreria standard
#include <pthread.h>//para usar threads
#include <semaphore.h>//para usar semaforos
#define ITERACIONES 10000

sem_t sem_descansar, sem_jugar, sem_ganar, sem_perder, sem_ultimoCiclo;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int sePuedeJugar;//variable que indica si ganar o perder pueden ejecutar su accion
int juegosGanados;
void* Jugar(void*p)
{
    int i;
    for(i = 0 ; i < ITERACIONES ; i++)
    {
        sem_wait(&sem_jugar);
        pthread_mutex_lock(&mutex);
        printf("JUEGO\n");
        sePuedeJugar = 1;//SOLO UNO PUEDE GANAR
        pthread_mutex_unlock(&mutex);
        sem_post(&sem_ganar);
        sem_post(&sem_perder);
    }
    pthread_exit(0);
}

void* Ganar(void*p)
{
    int i;
    for(i = 0 ; i < ITERACIONES ; i++)
    {
        sem_wait(&sem_ganar);//CONDICION DE CARRERA
        pthread_mutex_lock(&mutex);//CONDICION DE CARRERA
        if(sePuedeJugar == 0)//SI VALE 0 ES PORQUE 'PERDER' SE EJECUTO PRIMERO
        {
            sem_post(&sem_descansar);//ENTONCES HABILITO DESCANSAR
        }
        if(sePuedeJugar == 1)//SI VALE 1 ES PORQUE LLEGO PRIMERO EN LA CONDICION DE CARRERA
        {
            printf("GANAR\n");
            juegosGanados++;
            sePuedeJugar--;
        }
        pthread_mutex_unlock(&mutex);
    }
    pthread_exit(0);
}

void* Perder(void*p)
{
    int i;
    for(i = 0 ; i < ITERACIONES ; i++)
    {
        sem_wait(&sem_perder);//CONDICION DE CARRERA
        pthread_mutex_lock(&mutex);//CONDICION DE CARRERA
        if(sePuedeJugar == 0)//SI VALE 0 ES PORQUE 'GANAR' SE EJECUTO PRIMERO
        {
            sem_post(&sem_descansar);//ENTONCES HABILITO DESCANSAR
        }
        if(sePuedeJugar == 1)//SI VALE 1 ES PORQUE LLEGO PRIMERO EN LA CONDICION DE CARRERA
        {
            printf("PIERDO\n");
            sePuedeJugar--;
        }
        pthread_mutex_unlock(&mutex);
    }
    pthread_exit(0);
}

void* Descansar(void*p)
{
    int i;
    for(i = 0 ; i < ITERACIONES ; i++)
    {
        sem_wait(&sem_descansar);
        pthread_mutex_lock(&mutex);
        printf("DESCANSO\n");
        pthread_mutex_unlock(&mutex);
        sem_post(&sem_jugar);
    }
    pthread_mutex_lock(&mutex);
    sem_post(&sem_ultimoCiclo);
    pthread_mutex_unlock(&mutex);
    pthread_exit(0);
}

void* Terminar(void*p)
{
    sem_wait(&sem_ultimoCiclo);
    pthread_mutex_lock(&mutex);
    printf("TERMINO\n");
     int perdidos = ITERACIONES-juegosGanados;
    printf("Ganadas: %i Perdidas: %i\n",juegosGanados,perdidos);
    pthread_mutex_unlock(&mutex);
    pthread_exit(0);
}

int main()
{    
    sem_init(&sem_jugar,0,1);
    sem_init(&sem_ganar,0,0);
    sem_init(&sem_perder,0,0);
    sem_init(&sem_descansar,0,0);
    sem_init(&sem_ultimoCiclo,0,0);

    pthread_t t1, t2, t3, t4, t5;
    pthread_create(&t1,NULL,&Jugar,NULL);
    pthread_create(&t2,NULL,&Ganar,NULL);
    pthread_create(&t3,NULL,&Perder,NULL);
    pthread_create(&t4,NULL,&Descansar,NULL);
    pthread_create(&t5,NULL,&Terminar,NULL);
    
    pthread_join(t1,NULL);
    pthread_join(t2,NULL);
    pthread_join(t3,NULL);
    pthread_join(t4,NULL);
    pthread_join(t5,NULL);

    sem_destroy(&sem_descansar);
    sem_destroy(&sem_jugar);
    sem_destroy(&sem_ganar);
    sem_destroy(&sem_perder);
    sem_destroy(&sem_ultimoCiclo);
    pthread_mutex_destroy(&mutex);

    pthread_exit(0);
	return 0;
}
