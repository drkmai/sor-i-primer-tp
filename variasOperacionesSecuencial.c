#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

int sumaTotal=0;
int numberArray[10000];

int main()
{
  FILE *myFile;
  myFile = fopen("10milDigitosDePi_separados.txt", "r");

  //leer archivo en un array

  int i;

  if (myFile == NULL)
  {
    printf("Error en la lectura\n");
    exit (0);
  }

  for (i = 0; i < 10000; i++)
  {
    fscanf(myFile, "%d,", &numberArray[i] );
  }

    printf("Operaciones secuenciales\n");
    int suma = 0;
    int cantidadPrimos=0;
    int sumaPrimos =0;
    int aparicionDigitos[10] = {0,0,0,0,0,0,0,0,0,0};
    for(i = 0; i < 10000; i++)
    {
      int actual = numberArray[i];
      for (int j = 0; j < 10; j++)
      {
        if (actual == j)
        {
          aparicionDigitos[j]++;
        }
      }
      int cantidad=0;
      for (int k = 1; k<actual;k++)
      {
        if (actual%k==0)
        {
          cantidad++;
        }
      }
      if (cantidad==2)
      {
        cantidadPrimos++;
        sumaPrimos+=actual;
      }
        suma +=actual;
    }
    double promedio;
    int cantidadDigitos = 10000;
    promedio = (double)suma / (double)cantidadDigitos;
    printf("el promedio da %f\n",promedio);
    printf("la cantidad de digitos primos es %i\n",cantidadPrimos);
    printf("la suma de los digitos primos es %i\n",sumaPrimos);
    
    int max = 0;
    int index = -1;
    for (i = 0; i < 10;i++)
    {
      if (aparicionDigitos[i]>max)
      {
        max = aparicionDigitos[i];
        index = i;
      }
      printf("el digito %i aparece %i veces\n",i,aparicionDigitos[i]);
    }
     printf("el digito con mas apariciones es %i\n",index);

    fclose(myFile);

    return 0;
}