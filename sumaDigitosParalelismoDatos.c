#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

int sumaTotal=0;
int numberArray[10000];
pthread_mutex_t mutex;

void *sumar(void *limite)
{
	int hasta;
	int desde;
	desde = (int *)limite;
	hasta=desde+2000;
	int sumaParcial;
	sumaParcial=0;
	for (int i = desde; i<hasta;i++)
	{
		sumaParcial+=numberArray[i];
	}

	pthread_mutex_lock(&mutex);
	sumaTotal+=sumaParcial;
	pthread_mutex_unlock(&mutex);

	pthread_exit(0); 
}

int main()
{

	FILE *myFile;
	myFile = fopen("10milDigitosDePi_separados.txt", "r");

	//leer archivo en un array

	int i;

	if (myFile == NULL){
	printf("Error en la lectura\n");
	exit (0);
	}

	for (i = 0; i < 10000; i++){
	fscanf(myFile, "%d,", &numberArray[i] );
	}

	printf("suma con paralelismo de datos\n");
	pthread_mutex_init(&mutex,NULL);
	pthread_t t1,t2,t3,t4,t5;

	//malloc asigna la cantidad de memoria del tamaño requerido.
	int *arg1 = (int *) malloc(sizeof(arg1));
	arg1 = (int *)0;
	int *arg2 = (int *) malloc(sizeof(arg2));
	arg2 =(int *)2000;
	int *arg3 = (int *) malloc(sizeof(arg3));
	arg3 = (int *)4000;
	int  *arg4 = (int *) malloc(sizeof(arg4));
	arg4 = (int *) 6000;
	int *arg5 = (int *) malloc(sizeof(arg5));
	arg5 = (int *) 8000;

	pthread_create(&t1,NULL,&sumar,arg1);
	pthread_create(&t2,NULL,sumar,arg2);
	pthread_create(&t3,NULL,&sumar,arg3);
	pthread_create(&t4,NULL,&sumar,arg4);
	pthread_create(&t5,NULL,&sumar,arg5);

	pthread_join(t1,NULL);
	pthread_join(t2,NULL);
	pthread_join(t3,NULL);
	pthread_join(t4,NULL);
	pthread_join(t5,NULL);

	printf("la suma con paralelismo da %i\n",sumaTotal);

	pthread_mutex_destroy(&mutex);

  fclose(myFile);

  return 0;
}
